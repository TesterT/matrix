using System;
using System.Runtime.Serialization;

#nullable enable
namespace MatrixLibrary
{
    
    [Serializable]
    public class MatrixException : Exception
    {
        public MatrixException()
        {
        }

        public MatrixException(string message) : base(message)
        {
        }
        
        public MatrixException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }

        // Without this constructor, deserialization will fail
        protected MatrixException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }
    }
    
    public class Matrix : ICloneable
    {
        private enum Dimensions
        {
            Rows = 0,
            Columns = 1
        }

        public double[,] Array { get; }

        /// <summary>
        /// Number of rows.
        /// </summary>
        public int Rows
        {
            get => Array.GetLength((int) Dimensions.Rows);
        }

        /// <summary>
        /// Number of columns.
        /// </summary>
        public int Columns
        {
            get => Array.GetLength((int) Dimensions.Columns);
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix"/> class.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public Matrix(int rows, int columns)
        {
            
            if (rows <= 0) throw new ArgumentOutOfRangeException(nameof(rows), "Rows must be non-negative");
            if (columns <= 0) throw new ArgumentOutOfRangeException(nameof(columns), "Columns must be non-negative");
            Array = new double[rows, columns];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix"/> class with the specified elements.
        /// </summary>
        /// <param name="array">An array of floating-point values that represents the elements of this Matrix.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public Matrix(double[,] array)
        {
            Array = array ?? throw new ArgumentNullException(nameof(array));
        }

        /// <summary>
        /// Allows instances of a Matrix to be indexed just like arrays.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <exception cref="ArgumentException"></exception>
        public double this[int row, int column]
        {
            get
            {
                try
                {
                    return Array[row, column];
                }
                catch
                {
                    throw new ArgumentException("Index is incorrect.");
                }
            } 
            set
            {
                try
                {
                    Array[row, column] = value;
                }
                catch
                {
                    throw new ArgumentException("Index is incorrect.");
                }
            } 
        }

        /// <summary>
        /// Creates a deep copy of this Matrix.
        /// </summary>
        /// <returns>A deep copy of the current object.</returns>
        public object Clone()
        {
            return new Matrix(Array);
        }

        /// <summary>
        /// Adds two matrices.
        /// </summary>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns>New <see cref="Matrix"/> object which is sum of two matrices.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 == null) throw new ArgumentNullException(nameof(matrix1));
            if (matrix2 == null) throw new ArgumentNullException(nameof(matrix2));

            if (matrix1.Rows != matrix2.Rows || matrix1.Columns != matrix2.Columns)
            {
                throw new MatrixException("Matrix dimensions are different.");
            }

            var result = new Matrix(matrix1.Rows, matrix1.Columns);
            for (var i = 0; i < matrix1.Rows; i++)
            {
                for (var j = 0; j < matrix1.Columns; j++)
                {
                    result[i, j] = matrix1[i, j] + matrix2[i, j];
                }
            }

            return result;
        }

        /// <summary>
        /// Subtracts two matrices.
        /// </summary>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns>New <see cref="Matrix"/> object which is subtraction of two matrices</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public static Matrix operator -(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 == null) throw new ArgumentNullException(nameof(matrix1));
            if (matrix2 == null) throw new ArgumentNullException(nameof(matrix2));

            if (matrix1.Rows != matrix2.Rows || matrix1.Columns != matrix2.Columns)
            {
                throw new MatrixException("Matrix dimensions are different.");
            }

            var result = new Matrix(matrix1.Rows, matrix1.Columns);
            for (var i = 0; i < matrix1.Rows; i++)
            {
                for (var j = 0; j < matrix1.Columns; j++)
                {
                    result[i, j] = matrix1[i, j] - matrix2[i, j];
                }
            }

            return result;
        }

        /// <summary>
        /// Multiplies two matrices.
        /// </summary>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns>New <see cref="Matrix"/> object which is multiplication of two matrices.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public static Matrix operator *(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 == null) throw new ArgumentNullException(nameof(matrix1));
            if (matrix2 == null) throw new ArgumentNullException(nameof(matrix2));
            
            if (matrix1.Columns != matrix2.Rows)
            {
                throw new MatrixException();
            }

            var matrix1Array = matrix1.Array;
            var matrix2Array = matrix2.Array;
            var resultArray = new double[matrix1.Rows, matrix2.Columns];
            
            for (var i = 0; i < resultArray.GetLength((int) Dimensions.Rows); i++)
            {
                for (var j = 0; j < resultArray.GetLength((int) Dimensions.Columns); j++)
                {
                    resultArray[i, j] = 0;

                    for (var k = 0; k < matrix1Array.GetLength((int) Dimensions.Columns); k++)
                    {
                        resultArray[i, j] = resultArray[i, j] + matrix1Array[i, k] * matrix2Array[k, j];
                    }
                }
            }

            return new Matrix(resultArray);
        }

        /// <summary>
        /// Adds <see cref="Matrix"/> to the current matrix. 
        /// </summary>
        /// <param name="matrix"><see cref="Matrix"/> for adding.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public Matrix Add(Matrix matrix)
        {
            return this + matrix;
        }

        /// <summary>
        /// Subtracts <see cref="Matrix"/> from the current matrix.
        /// </summary>
        /// <param name="matrix"><see cref="Matrix"/> for subtracting.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public Matrix Subtract(Matrix matrix)
        {
            return this - matrix;
        }

        /// <summary>
        /// Multiplies <see cref="Matrix"/> on the current matrix.
        /// </summary>
        /// <param name="matrix"><see cref="Matrix"/> for multiplying.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixException"></exception>
        public Matrix Multiply(Matrix matrix)
        {
            return this * matrix;
        }

        /// <summary>
        /// Determines whether the two specified <see cref="Matrix"/> structures have the same values.
        /// </summary>
        /// <param name="obj">The object to compare.</param>
        /// <returns>True if matrices are equal, false if are not equal.</returns>
        public override bool Equals(object? obj)
        {
            if (!(obj is Matrix matrix))
            {
                return false;
            }
        
            if (matrix.Columns != Columns || matrix.Rows != Rows)
            {
                return false;
            }

            for (var i = 0; i < matrix.Rows; i++)
            {
                for (var j = 0; j < matrix.Columns; j++)
                {
                    if (Math.Abs(this[i, j] - matrix[i, j]) > 0.001)
                    {
                        return false;
                    }
                }
            }
        
            return true;
        }
        
        public override int GetHashCode() => Array.GetHashCode() + Columns.GetHashCode() + Rows.GetHashCode();
    }
}
